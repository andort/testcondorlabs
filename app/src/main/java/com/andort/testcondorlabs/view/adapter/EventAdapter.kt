package com.andort.testcondorlabs.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.andort.testcondorlabs.models.Event
import com.andort.testcondorlabs.viewmodel.EventViewModel
import com.andort.userexamplemvvm.R
import com.andort.userexamplemvvm.databinding.ItemEventBinding

class EventAdapter(var list: ArrayList<Event>) : RecyclerView.Adapter<EventAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemEventBinding) : RecyclerView.ViewHolder(binding.root){

        val eventViewModel = EventViewModel()

        fun bind(event: Event){
            eventViewModel.bind(event)
            binding.itemEventViewModel = eventViewModel
        }
    }

    fun updateList(list: ArrayList<Event>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemEventBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_event, parent, false)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }


}