package com.andort.testcondorlabs.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.andort.testcondorlabs.base.BaseActivity
import com.andort.userexamplemvvm.R
import com.andort.testcondorlabs.viewmodel.HomeViewModel
import com.andort.userexamplemvvm.databinding.ActivityHomeBinding
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: ActivityHomeBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.viewModel = viewModel
        setAdapter()

        viewModel.getTeams("Spanish La Liga")

        option_esp.setOnClickListener {
            viewModel.getTeams("Spanish La Liga")
        }

        option_bundes.setOnClickListener {
            viewModel.getTeams("German Bundesliga")
        }

        option_premier.setOnClickListener {
            viewModel.getTeams("English Premier League")
        }

    }


    private fun setAdapter(){
        binding.catalogueList.layoutManager = LinearLayoutManager(this,
            LinearLayoutManager.VERTICAL,
            false)
    }


}
