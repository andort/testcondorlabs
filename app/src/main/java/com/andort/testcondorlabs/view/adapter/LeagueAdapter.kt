package com.andort.testcondorlabs.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.andort.testcondorlabs.models.Team
import com.andort.testcondorlabs.viewmodel.TeamViewModel
import com.andort.userexamplemvvm.R
import com.andort.userexamplemvvm.databinding.ItemLeagueBinding

class LeagueAdapter(var list: ArrayList<Team>) : RecyclerView.Adapter<LeagueAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemLeagueBinding) : RecyclerView.ViewHolder(binding.root){

        val leagueViewModel = TeamViewModel()

        fun bind(team: Team){
            leagueViewModel.bind(team)
            binding.itemLeaguaViewModel = leagueViewModel
        }
    }

    fun updateList(list: ArrayList<Team>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemLeagueBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_league, parent, false)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }


}