package com.andort.testcondorlabs.view.activity

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.andort.testcondorlabs.base.BaseActivity
import com.andort.testcondorlabs.models.Team
import com.andort.testcondorlabs.viewmodel.TeamDetailViewModel
import com.andort.userexamplemvvm.R
import com.andort.userexamplemvvm.databinding.ActivityDetailBinding
import kotlinx.android.synthetic.main.activity_detail.*

class TeamDetailActivity : BaseActivity() {

    private lateinit var viewModel: TeamDetailViewModel
    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(TeamDetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.detailViewModel = viewModel
        setAdapter()

        viewModel.team.value = team

        viewModel.setLabels(team)



        iv_in.setOnClickListener {

            var urlInstagram: String = "http://"+ team.strInstagram
            val uri = Uri.parse(urlInstagram)
            val likeIng = Intent(Intent.ACTION_VIEW, uri)
            likeIng.setPackage("com.instagram.android")

            try {
                startActivity(likeIng)
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(urlInstagram)
                    )
                )
            }
        }

        iv_tw.setOnClickListener {
            var intent: Intent? = null
            var urlTwitter: String = "https://"+team.strTwitter

            try { // get the Twitter app if possible
                this.packageManager.getPackageInfo("com.twitter.android", 0)
                intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(urlTwitter)
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            } catch (e: Exception) { // no Twitter app, revert to browser
                intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(urlTwitter)
                )
            }
            this.startActivity(intent)
        }

        iv_fb.setOnClickListener {
            val uri = "http://"+team.strFacebook
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            startActivity(intent)
        }

        iv_web.setOnClickListener {

            var url: String = ""
            if (team.strWebsite != null){
                url = "http://"+team.strWebsite
            }

            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(url)
            startActivity(openURL)
        }
    }

    private fun setAdapter(){
        binding.eventList.layoutManager = LinearLayoutManager(this,
            LinearLayoutManager.VERTICAL,
            false)
    }


    companion object {
        lateinit var team: Team
        fun newIntent(context: Context, team: Team): Intent {
            this.team = team
            return Intent(context, TeamDetailActivity::class.java)

        }
    }
}
