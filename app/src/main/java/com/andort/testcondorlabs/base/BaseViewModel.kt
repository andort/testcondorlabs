package com.andort.testcondorlabs.base

import androidx.lifecycle.ViewModel
import com.andort.testcondorlabs.injection.component.IViewModelInjector
import com.andort.testcondorlabs.injection.module.ViewModelModule
import com.andort.testcondorlabs.viewmodel.HomeViewModel
import com.andort.testcondorlabs.injection.component.DaggerIViewModelInjector
import com.andort.testcondorlabs.viewmodel.TeamDetailViewModel

open class BaseViewModel: ViewModel() {

    private val injectorI: IViewModelInjector = DaggerIViewModelInjector
        .builder()
        .viewModelModule(ViewModelModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is HomeViewModel -> injectorI.inject(this)
            is TeamDetailViewModel -> injectorI.inject(this)
        }
    }

}