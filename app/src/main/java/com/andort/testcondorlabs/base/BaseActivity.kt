package com.andort.testcondorlabs.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TestApplication.currentActivity = this    }

    override fun onPause() {
        super.onPause()
        TestApplication.isInForeGround = false
    }

    override fun onPostResume() {
        super.onPostResume()
        TestApplication.isInForeGround = true
        TestApplication.currentActivity = this
    }
}