package com.andort.testcondorlabs.base

import android.annotation.SuppressLint
import android.app.Application
import androidx.appcompat.app.AppCompatActivity

class TestApplication: Application() {

    companion object {

        var isInForeGround: Boolean = false

        @SuppressLint("StaticFieldLeak")
        lateinit var currentActivity: AppCompatActivity

        @SuppressLint("StaticFieldLeak")
        lateinit var instance: TestApplication
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }


}
