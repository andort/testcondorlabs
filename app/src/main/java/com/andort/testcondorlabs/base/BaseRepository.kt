package com.andort.testcondorlabs.base

import com.andort.testcondorlabs.injection.component.IRepositoryInjector
import com.andort.testcondorlabs.injection.module.RepositoryModule
import com.andort.testcondorlabs.injection.component.DaggerIRepositoryInjector
import com.andort.testcondorlabs.repositories.TeamDetailRepository
import com.andort.testcondorlabs.repositories.HomeRepository

abstract class BaseRepository {

    private val injectorI: IRepositoryInjector = DaggerIRepositoryInjector
        .builder()
        .repositoryModule(RepositoryModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is HomeRepository -> injectorI.inject(this)
            is TeamDetailRepository -> injectorI.inject(this)
        }
    }
}