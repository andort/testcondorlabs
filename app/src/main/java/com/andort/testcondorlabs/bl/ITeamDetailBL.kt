package com.andort.testcondorlabs.bl

import com.andort.testcondorlabs.models.EventsResponse
import io.reactivex.Observable

interface ITeamDetailBL {

    fun getEvent(idEvent: String): Observable<EventsResponse>
}