package com.andort.testcondorlabs.bl

import com.andort.testcondorlabs.models.LeaguesResponse
import com.andort.testcondorlabs.models.TeamsByLeagueResponse
import io.reactivex.Observable

interface IHomeBL {

    fun getLeague(): Observable<LeaguesResponse>

    fun getTeam(league: String): Observable<TeamsByLeagueResponse>
}