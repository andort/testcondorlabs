package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Event (val idEvent: String,
            val strEvent: String,
            val strFilename: String,
            val strLeague: String,
            val strHomeTeam: String,
            val strAwayTeam: String,
            val dateEvent: String,
            val strTimeLocal: String): Parcelable