package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class TeamsByLeagueResponse (val teams: List<Team>): Parcelable