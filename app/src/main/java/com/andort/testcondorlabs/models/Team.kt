package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Team (val idTeam: String,
            val strTeam: String,
            val strAlternate: String,
            val strStadium: String,
            val strStadiumThumb: String,
            val intStadiumCapacity: String,
            val strTeamBadge: String,
            val strTeamJersey: String,
            val strDescriptionES: String,
            val strDescriptionEN: String,
            val intFormedYear: String,
            val strWebsite: String,
            val strFacebook: String,
            val strTwitter: String,
            val strInstagram: String,
            val strYoutube: String): Parcelable