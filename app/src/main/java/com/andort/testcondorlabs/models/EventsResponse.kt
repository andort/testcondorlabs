package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class EventsResponse (val events: List<Event>): Parcelable


