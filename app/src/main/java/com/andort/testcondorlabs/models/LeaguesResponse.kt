package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class LeaguesResponse (val leagues: List<League>): Parcelable