package com.andort.testcondorlabs.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class League (val idLeague: String,
              val strLeague: String,
              val strSport: String,
              val strLeagueAlternate: String): Parcelable