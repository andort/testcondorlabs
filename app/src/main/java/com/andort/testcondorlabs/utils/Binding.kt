package com.andort.testcondorlabs.utils

import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


/**
 * @view : target text view, that will set text
 * @text : mutable text content
 * Show a mutable text --> example : app:mutableText
 */
@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
    }
}

/**
 * @view : target view
 * @visibility : view state --> example : View.GONE, View.VISIBLE
 * Set mutable state view --> example : app:mutableVisibility
 */
@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}

/**
 * @view : target recycler view
 * @adapter : Recycler view adapter implementation --> example : View.GONE, View.VISIBLE
 * Set mutable adapter --> example : app:adapter
 */
@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

/**
 * @url : url text, that will show
 * Extension ImageView class to show url content image with picasso
 */
@BindingAdapter("imageUrl")
fun ImageView.loadUrl(url: String?) {
    val parentActivity: AppCompatActivity? = this.getParentActivity()
    if (parentActivity != null && url != null) {
        Glide.with(parentActivity).load(url).into(this)
    }
}

/**
 * @view : target  view
 * @style : Int style reference to Drawable resource
 * Set custom mutable style  --> example : app:mutableStyle
 */
@Suppress("DEPRECATION")
@BindingAdapter("mutableStyle")
fun setStyle(view: View, style: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && style != null) {
        style.observe(parentActivity, Observer { value ->
            view.background =
                parentActivity.resources.getDrawable(value!!)
        })
    }
}

/**
 * @view : target view
 * @color : Int Color reference to resource
 * Set custom mutable text color --> example : app:mutableColor
 */
@Suppress("DEPRECATION")
@BindingAdapter("mutableColor")
fun setColor(view: TextView, color: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && color != null) {
        color.observe(parentActivity, Observer { value ->
            view.setTextColor(
                parentActivity.resources.getColor(value!!))
        })
    }
}

/**
 * @url : url text, that will show
 * Extension ImageView class to show url content image with picasso
 */
@BindingAdapter("bitmap")
fun ImageView.bitmap(image: Bitmap?) {
    val parentActivity: AppCompatActivity? = this.getParentActivity()
    if (parentActivity != null && image != null) {
        this.setImageBitmap(image)
    }
}

/**
 * @view : target view
 * @visibility : view state --> example : View.GONE, View.VISIBLE
 * Set mutable state view --> example : app:mutableVisibility
 */
@BindingAdapter("mutableCheck")
fun setMutableCheckState(switch: Switch, checked: MutableLiveData<Boolean>?) {
    val parentActivity: AppCompatActivity? = switch.getParentActivity()
    if (parentActivity != null && checked != null) {
        checked.observe(parentActivity, Observer { value -> switch.isChecked = value ?: switch.isChecked })
    }
}

