package com.andort.testcondorlabs.utils

import com.andort.userexamplemvvm.BuildConfig

object Constants {

    val BASE_URL = BuildConfig.BASE_URL
}