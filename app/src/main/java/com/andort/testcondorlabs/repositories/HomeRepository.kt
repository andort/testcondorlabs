package com.andort.testcondorlabs.repositories

import com.andort.testcondorlabs.base.BaseRepository
import com.andort.testcondorlabs.bl.IHomeBL
import com.andort.testcondorlabs.models.LeaguesResponse
import com.andort.testcondorlabs.models.TeamsByLeagueResponse
import com.andort.testcondorlabs.network.IHomeApi
import io.reactivex.Observable
import javax.inject.Inject

class HomeRepository: IHomeBL, BaseRepository() {

    @Inject
    lateinit var iHomeApi: IHomeApi

    override fun getLeague(): Observable<LeaguesResponse> {
        return iHomeApi.getLeague()
    }

    override fun getTeam(league: String): Observable<TeamsByLeagueResponse> {
        return iHomeApi.getTeam(league)
    }
}