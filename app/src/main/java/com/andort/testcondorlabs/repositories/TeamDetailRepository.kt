package com.andort.testcondorlabs.repositories

import com.andort.testcondorlabs.base.BaseRepository
import com.andort.testcondorlabs.bl.ITeamDetailBL
import com.andort.testcondorlabs.models.EventsResponse
import com.andort.testcondorlabs.network.ITeamDetailApi
import io.reactivex.Observable
import javax.inject.Inject

class TeamDetailRepository: ITeamDetailBL, BaseRepository() {

    @Inject
    lateinit var iTeamDetailApi: ITeamDetailApi

    override fun getEvent(idEvent: String): Observable<EventsResponse> {
        return iTeamDetailApi.getEvent(idEvent)
    }

}