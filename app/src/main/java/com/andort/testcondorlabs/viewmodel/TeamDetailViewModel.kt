package com.andort.testcondorlabs.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.andort.testcondorlabs.base.BaseViewModel
import com.andort.testcondorlabs.bl.ITeamDetailBL
import com.andort.testcondorlabs.models.Event
import com.andort.testcondorlabs.models.EventsResponse
import com.andort.testcondorlabs.models.Team
import com.andort.testcondorlabs.view.adapter.EventAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TeamDetailViewModel: BaseViewModel()  {

    @Inject
    lateinit var teamDetailRepository: ITeamDetailBL
    private var subscription: Disposable? = null

    var team = MutableLiveData<Team>()

    var teamName = MutableLiveData<String>()
    var teamEmblem = MutableLiveData<String>()
    var foundedTeam = MutableLiveData<String>()
    var description = MutableLiveData<String>()
    var teamJersey = MutableLiveData<String>()

    var showTw = MutableLiveData<Int>()
    var showIn = MutableLiveData<Int>()
    var showFb = MutableLiveData<Int>()
    var showWeb = MutableLiveData<Int>()

    var eventList = ArrayList<Event>()
    var adapter = EventAdapter(eventList)

    fun setLabels(team: Team){
        teamName.value  = team.strTeam
        teamEmblem.value  = team.strTeamBadge
        foundedTeam.value  = "Fundada en " + team.intFormedYear

        if(team.strDescriptionES != null){
            description.value  = team.strDescriptionES
        } else {
            description.value  = team.strDescriptionEN
        }

        teamJersey.value = team.strTeamJersey

        if(team.strTwitter == ""){
            showTw.value = View.GONE
        }
        if(team.strInstagram == ""){
            showIn.value = View.GONE
        }
        if(team.strFacebook == ""){
            showFb.value = View.GONE
        }
        if(team.strWebsite == ""){
            showWeb.value = View.GONE
        }

        getEvent(team.idTeam)
    }

    private fun getEvent(idEvent: String) {
        subscription = teamDetailRepository.getEvent(idEvent)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> onRetrieveEventResponse(result) },
                { error -> onRetrieveEventError(error) }
            )
    }

    private fun onRetrieveEventResponse(result: EventsResponse?) {
        eventList = result?.events as ArrayList<Event>
        adapter.updateList(eventList)
        adapter.notifyDataSetChanged()
    }

    private fun onRetrieveEventError(error: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}