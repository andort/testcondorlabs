package com.andort.testcondorlabs.viewmodel

import androidx.lifecycle.MutableLiveData
import com.andort.testcondorlabs.base.BaseViewModel
import com.andort.testcondorlabs.models.Event

class EventViewModel: BaseViewModel() {

    var homeTeam = MutableLiveData<String>()
    var awayTeam = MutableLiveData<String>()
    var eventData = MutableLiveData<String>()

    lateinit var event: Event

    fun bind(event: Event) {
        homeTeam.value = event.strHomeTeam
        awayTeam.value = event.strAwayTeam
        eventData.value = "Fecha " + event.dateEvent + ", hora " + event.strTimeLocal
        this.event = event
    }

}