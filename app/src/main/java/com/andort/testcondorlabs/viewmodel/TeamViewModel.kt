package com.andort.testcondorlabs.viewmodel

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.andort.testcondorlabs.base.BaseViewModel
import com.andort.testcondorlabs.base.TestApplication
import com.andort.testcondorlabs.models.Team
import com.andort.testcondorlabs.view.activity.TeamDetailActivity

class TeamViewModel: BaseViewModel() {

    var teamName = MutableLiveData<String>()
    var stadiumName = MutableLiveData<String>()
    var teamEmblem = MutableLiveData<String>()

    lateinit var team: Team

    fun bind(team: Team){
        teamName.value = team.strTeam
        stadiumName.value = team.strStadium
        teamEmblem.value = team.strTeamBadge
        this.team = team
    }

    fun goTeamDetail(){
        val intent : Intent = TeamDetailActivity.newIntent(TestApplication.currentActivity, team)
        TestApplication.currentActivity.startActivity(intent)
    }

}