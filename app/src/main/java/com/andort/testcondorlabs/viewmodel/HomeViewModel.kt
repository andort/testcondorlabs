package com.andort.testcondorlabs.viewmodel

import android.util.Log
import com.andort.testcondorlabs.base.BaseViewModel
import com.andort.testcondorlabs.bl.IHomeBL
import com.andort.testcondorlabs.models.League
import com.andort.testcondorlabs.models.LeaguesResponse
import com.andort.testcondorlabs.models.Team
import com.andort.testcondorlabs.models.TeamsByLeagueResponse
import com.andort.testcondorlabs.view.adapter.LeagueAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel : BaseViewModel() {

   @Inject
   lateinit var homeRepository: IHomeBL
   private var subscription: Disposable? = null
   var leaguesList = ArrayList<League>()
   var teamList = ArrayList<Team>()
   var adapter = LeagueAdapter(teamList)


   fun getLeague() {
      subscription = homeRepository.getLeague()
         .subscribeOn(Schedulers.io())
         .observeOn(AndroidSchedulers.mainThread())
         .subscribe(
            { result -> onRetrieveLeaguesResponse(result) },
            { error -> onRetrieveLeaguesError(error) }
         )
   }

   fun getTeams(team: String) {
      subscription = homeRepository.getTeam(team)
         .subscribeOn(Schedulers.io())
         .observeOn(AndroidSchedulers.mainThread())
         .subscribe(
            { result -> onRetrieveTeamResponse(result) },
            { error -> onRetrieveTeamError(error) }
         )
   }

   private fun onRetrieveTeamResponse(result: TeamsByLeagueResponse?) {
      teamList = result?.teams as ArrayList<Team>
      adapter.updateList(teamList)
      adapter.notifyDataSetChanged()
   }

   private fun onRetrieveTeamError(error: Throwable?) {
      if (!error!!.message.isNullOrBlank()){
         Log.e("HomeViewModel ", error!!.message)
      }
   }


   private fun onRetrieveLeaguesResponse(result: LeaguesResponse?) {
      leaguesList = result?.leagues as ArrayList<League>
      //adapter.updateList(leaguesList)
      //adapter.notifyDataSetChanged()
   }

   private fun onRetrieveLeaguesError(error: Throwable?) {

      if (!error!!.message.isNullOrBlank()){
         Log.e("HomeViewModel ", error!!.message)
      }

   }



}