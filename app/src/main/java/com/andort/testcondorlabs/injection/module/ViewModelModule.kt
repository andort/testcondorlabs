package com.andort.testcondorlabs.injection.module

import com.andort.testcondorlabs.bl.IHomeBL
import com.andort.testcondorlabs.bl.ITeamDetailBL
import com.andort.testcondorlabs.repositories.TeamDetailRepository
import com.andort.testcondorlabs.repositories.HomeRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable


@Module
@Suppress("unused")
object ViewModelModule {

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideLoginRepository(): IHomeBL {
        return HomeRepository()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideEventRepository(): ITeamDetailBL {
        return TeamDetailRepository()
    }



}