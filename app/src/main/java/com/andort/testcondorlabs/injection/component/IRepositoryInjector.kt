package com.andort.testcondorlabs.injection.component

import com.andort.testcondorlabs.injection.module.RepositoryModule
import com.andort.testcondorlabs.repositories.TeamDetailRepository
import com.andort.testcondorlabs.repositories.HomeRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(RepositoryModule::class)])
interface IRepositoryInjector {

    fun inject(homeRepository: HomeRepository)
    fun inject(eventRepository:  TeamDetailRepository)

    @Component.Builder
    interface Builder {
        fun build(): IRepositoryInjector

        fun repositoryModule(repositoryModule: RepositoryModule): Builder
    }
}