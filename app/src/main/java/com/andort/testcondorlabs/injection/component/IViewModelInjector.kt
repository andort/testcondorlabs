package com.andort.testcondorlabs.injection.component

import com.andort.testcondorlabs.injection.module.ViewModelModule
import com.andort.testcondorlabs.viewmodel.EventViewModel
import com.andort.testcondorlabs.viewmodel.HomeViewModel
import com.andort.testcondorlabs.viewmodel.TeamDetailViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(ViewModelModule::class)])
interface IViewModelInjector {

    fun inject(HomeViewModel: HomeViewModel)
    fun inject(teamDetailViewModel: TeamDetailViewModel)


    @Component.Builder
    interface Builder {
        fun build(): IViewModelInjector

        fun viewModelModule(viewModelModule: ViewModelModule): Builder
    }
}