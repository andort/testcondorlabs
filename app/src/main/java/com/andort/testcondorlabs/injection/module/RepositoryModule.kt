package com.andort.testcondorlabs.injection.module

import com.andort.testcondorlabs.network.ITeamDetailApi
import com.andort.testcondorlabs.network.IHomeApi
import com.andort.testcondorlabs.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

@Module
@Suppress("unused")
object RepositoryModule {


    @Provides
    @Reusable
    @JvmStatic
    internal fun provideHomeApi(retrofit: Retrofit): IHomeApi {
        return retrofit.create(IHomeApi::class.java)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideEventApi(retrofit: Retrofit): ITeamDetailApi {
        return retrofit.create(ITeamDetailApi::class.java)
    }



    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        val httpClient = OkHttpClient.Builder()
        httpClient.networkInterceptors().add(Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
            requestBuilder.header("Content-Type", "application/json")
            //requestBuilder.header("Ocp-Apim-Subscription-Key", Constants.SUB_KEY)
            //requestBuilder.header("api-version", Constants.API_VERSION)

            chain.proceed(requestBuilder.build())
        })

        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient.build())
            .build()
    }


}