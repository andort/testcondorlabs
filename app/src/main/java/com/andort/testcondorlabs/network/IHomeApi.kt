package com.andort.testcondorlabs.network

import com.andort.testcondorlabs.models.LeaguesResponse
import com.andort.testcondorlabs.models.TeamsByLeagueResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface IHomeApi {

    @GET("json/1/all_leagues.php")
    fun getLeague(): Observable<LeaguesResponse>

    @GET("json/1/search_all_teams.php")
    fun getTeam(@Query("l") league: String): Observable<TeamsByLeagueResponse>

}