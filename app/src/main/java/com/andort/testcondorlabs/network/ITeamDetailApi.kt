package com.andort.testcondorlabs.network

import com.andort.testcondorlabs.models.EventsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ITeamDetailApi {

    @GET("json/1/eventsnext.php")
    fun getEvent(@Query("id") idEvent: String): Observable<EventsResponse>
}